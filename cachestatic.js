$(document).ready(function() {
	$('#closeVCache').click(function() {
		//$.get('tonfichier.php');
		document.getElementById("vcache").style.display="none";
		document.getElementById("vcache-closed").style.display="block";
	});

	$('#vcache-closed').click(function() {
		//$.get('tonfichier.php');
		document.getElementById("vcache").style.display="block";
		document.getElementById("vcache-closed").style.display="none";
	});	

	$('#renewCache').click(function() {
		$.get('/vcache/removeCache');
		alert('The cache for this page was well deleted.');
		window.location.reload(true); 
	});		
	
	$('#setNoCache').click(function() {
		$.get('/vcache/setNoCache');
		$.get('/vcache/removeCache');
		alert('The cache for this page was been disabled.');
		window.location.reload(true); 
	});		
	
	$('#unsetNoCache').click(function() {
		$.get('/vcache/unsetNoCache');
		$.get('/vcache/removeCache');
		alert('The cache for this page was been enabled.');
		window.location.reload(true); 
	});
});
